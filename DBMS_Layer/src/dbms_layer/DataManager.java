/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbms_layer;

import data_layer.Task;
import data_layer.User;
import dbms_layer.contract.IDataManager;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.xml.stream.XMLStreamException;

/**
 *
 * @author 2882_tsukanov
 */
public class DataManager {

    IDataManager dataManager;

    public DataManager(IDataManager dataManager) {
        this.dataManager = dataManager;
    }

    public void createTask(Task task) {
        dataManager.createTask(task);
    }

    public void createTasks(List<Task> tasks) throws IOException, FileNotFoundException, XMLStreamException {
        dataManager.createTasks(tasks);
    }

    public void createUser(User user) {
        dataManager.createUser(user);
    }

    public void createUsers(List<User> users) throws IOException, FileNotFoundException, XMLStreamException {
        dataManager.createUsers(users);
    }

    public Task selectTask() {
        return dataManager.selectTask();
    }

    public List<Task> selectTasks() {
        return dataManager.selectTasks();
    }

    public User selectUser() {
        return dataManager.selectUser();
    }

    public List<User> selectUsers() {
        return dataManager.selectUsers();
    }

}
