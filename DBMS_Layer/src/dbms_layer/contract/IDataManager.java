/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbms_layer.contract;

import data_layer.Task;
import data_layer.User;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import javax.xml.stream.XMLStreamException;

/**
 *
 * @author 2882_tsukanov
 */
public interface IDataManager {

    public void createTask(Task task);

    public void createTasks(List<Task> tasks) throws FileNotFoundException, IOException, XMLStreamException;

    public void createUser(User user);

    public void createUsers(List<User> users) throws FileNotFoundException, IOException, XMLStreamException;

    public Task selectTask();

    public List<Task> selectTasks();

    public User selectUser();

    public List<User> selectUsers();

}
