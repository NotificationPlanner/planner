/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbms_layer.realization;

import com.sun.org.apache.xerces.internal.impl.PropertyManager;
import com.sun.org.apache.xerces.internal.impl.XMLStreamReaderImpl;
import com.sun.xml.internal.stream.writers.XMLStreamWriterImpl;
import data_layer.Task;
import data_layer.User;
import data_layer.task.*;
import data_layer.user.*;
import dbms_layer.contract.IDataManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.Location;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

/**
 *
 * @author 2882_tsukanov
 */
public class XMLDataManager implements IDataManager {

    String pathUser = "XMLUserData.xml";
    String pathTask = "XMLTaskData.xml";
    String pathUsers = "XMLUsersData.xml";
    String pathTasks = "XMLTasksData.xml";

    public XMLDataManager(/*String path*/) {
        //this.path = path;
    }

    //Create XML Methods
    @Override
    public void createTask(Task task) {
        try {
            JAXBContext context = JAXBContext.newInstance(Task.class, TaskId.class, TaskData.class, TaskDate.class, TaskTitle.class, Notification.class, UserId.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            File file = new File(pathTask);
            marshaller.marshal(task, file);
            marshaller.marshal(task, System.out);
        } catch (JAXBException exception) {
            Logger.getLogger(XMLDataManager.class.getName()).
                    log(Level.SEVERE, "marshallExample throw JAXBException", exception);
        }
    }

    @Override
    public void createTasks(List<Task> tasks) throws FileNotFoundException, IOException, XMLStreamException {
        Iterator iterator = tasks.iterator();
        File file = new File(pathTasks);
        OutputStream outputStream = new FileOutputStream(file);
        PropertyManager propertyManager = new PropertyManager(PropertyManager.CONTEXT_WRITER);
        XMLStreamWriter streamWriter = new XMLStreamWriterImpl(outputStream, propertyManager);
        streamWriter.writeStartDocument("UTF-8", null);
        streamWriter.writeStartElement("tasks");
        while (iterator.hasNext()) {
            try {
                Task task = (Task) iterator.next();
                JAXBContext context = JAXBContext.newInstance(Task.class, TaskId.class, TaskData.class, TaskDate.class, TaskTitle.class, Notification.class, UserId.class);
                Marshaller marshaller = context.createMarshaller();
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                marshaller.marshal(task, streamWriter);
                //marshaller.marshal(task, System.out);
            } catch (JAXBException exception) {
                Logger.getLogger(XMLDataManager.class.getName()).
                        log(Level.SEVERE, "marshallExample throw JAXBException", exception);
            }
        }
        streamWriter.writeEndDocument();
        streamWriter.close();
    }

    @Override
    public void createUser(User user) {
        try {
            JAXBContext context = JAXBContext.newInstance(User.class, UserId.class, FullName.class, UserAccount.class, UserPassword.class, TaskId.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            File file = new File(pathUser);
            marshaller.marshal(user, file);
            marshaller.marshal(user, System.out);
        } catch (JAXBException exception) {
            Logger.getLogger(XMLDataManager.class.getName()).
                    log(Level.SEVERE, "marshallExample throw JAXBException", exception);
        }
    }

    @Override
    public void createUsers(List<User> users) throws FileNotFoundException, IOException, XMLStreamException {
        Iterator iterator = users.iterator();
        File file = new File(pathUsers);
        OutputStream outputStream = new FileOutputStream(file);
        PropertyManager propertyManager = new PropertyManager(PropertyManager.CONTEXT_WRITER);
        XMLStreamWriter streamWriter = new XMLStreamWriterImpl(outputStream, propertyManager);
        streamWriter.writeStartDocument("UTF-8", null);
        streamWriter.writeStartElement("users");
        while (iterator.hasNext()) {
            try {
                User user = (User) iterator.next();
                JAXBContext context = JAXBContext.newInstance(User.class, UserId.class, FullName.class, UserAccount.class, UserPassword.class, TaskId.class);
                Marshaller marshaller = context.createMarshaller();
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                marshaller.marshal(user, streamWriter);
                //marshaller.marshal(task, System.out);
            } catch (JAXBException exception) {
                Logger.getLogger(XMLDataManager.class.getName()).
                        log(Level.SEVERE, "marshallExample throw JAXBException", exception);
            }
        }
        streamWriter.writeEndDocument();
        streamWriter.close();
    }

    //Read XML and deserialization objects
    @Override
    public Task selectTask() {
        {
            Task task = null;
            try {
                JAXBContext context = JAXBContext.newInstance(Task.class, TaskId.class, TaskData.class, TaskDate.class, TaskTitle.class, Notification.class, UserId.class);

                File file = new File(pathTask);
                Unmarshaller unmarshaller = context.createUnmarshaller();
                Object object = unmarshaller.unmarshal(file);

                //prove the Group is recreated
                task = (Task) object;

            } catch (JAXBException exception) {
                Logger.getLogger(XMLDataManager.class.getName()).
                        log(Level.SEVERE, "marshallExample throw JAXBException", exception);
            }
            return task;
        }
    }

    @Override
    public List<Task> selectTasks() {
        List<Task> tasks = new LinkedList<>();
        File file = new File(pathTasks);
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(XMLDataManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        PropertyManager propertyManager = new PropertyManager(PropertyManager.CONTEXT_READER);
        XMLStreamReader streamReader = null;
        try {
            streamReader = new XMLStreamReaderImpl(inputStream, propertyManager);
        } catch (XMLStreamException ex) {
            Logger.getLogger(XMLDataManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            streamReader.nextTag();
        } catch (XMLStreamException ex) {
            Logger.getLogger(XMLDataManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            while ((streamReader.getLocalName().equals("tasks") && !streamReader.isEndElement()) || streamReader.getLocalName().equals("task")) {
                while (!streamReader.getLocalName().equals("task")) {
                    streamReader.nextTag();
                }

                JAXBContext context = JAXBContext.newInstance(Task.class, TaskId.class, TaskData.class, TaskDate.class, TaskTitle.class, Notification.class, UserId.class);

                Unmarshaller unmarshaller = context.createUnmarshaller();
                JAXBElement<Task> element = unmarshaller.unmarshal(streamReader, Task.class);

                Task task = element.getValue();
                tasks.add(task);
            }
            streamReader.close();

        } catch (JAXBException exception) {
            Logger.getLogger(XMLDataManager.class.getName()).
                    log(Level.SEVERE, "marshallExample throw JAXBException", exception);
        } catch (XMLStreamException ex) {
            Logger.getLogger(XMLDataManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tasks;
    }

    @Override
    public User selectUser() {
        {
            User user = null;
            try {
                JAXBContext context = JAXBContext.newInstance(User.class, UserId.class, FullName.class, UserAccount.class, UserPassword.class, TaskId.class);

                File file = new File(pathUser);
                Unmarshaller unmarshaller = context.createUnmarshaller();
                Object object = unmarshaller.unmarshal(file);

                //prove the Group is recreated
                user = (User) object;

            } catch (JAXBException exception) {
                Logger.getLogger(XMLDataManager.class.getName()).
                        log(Level.SEVERE, "marshallExample throw JAXBException", exception);
            }
            return user;
        }
    }

    @Override
    public List<User> selectUsers() {
        List<User> users = new LinkedList<>();
        File file = new File(pathUsers);
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(XMLDataManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        PropertyManager propertyManager = new PropertyManager(PropertyManager.CONTEXT_READER);
        XMLStreamReader streamReader = null;
        try {
            streamReader = new XMLStreamReaderImpl(inputStream, propertyManager);
        } catch (XMLStreamException ex) {
            Logger.getLogger(XMLDataManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            streamReader.nextTag();
        } catch (XMLStreamException ex) {
            Logger.getLogger(XMLDataManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            while ((streamReader.getLocalName().equals("users") && !streamReader.isEndElement()) || streamReader.getLocalName().equals("user")) {
                while (!streamReader.getLocalName().equals("user")) {
                    streamReader.nextTag();
                }

                JAXBContext context = JAXBContext.newInstance(User.class, UserId.class, FullName.class, UserAccount.class, UserPassword.class, TaskId.class);

                Unmarshaller unmarshaller = context.createUnmarshaller();
                JAXBElement<User> element = unmarshaller.unmarshal(streamReader, User.class);

                User user = element.getValue();
                users.add(user);
            }
            streamReader.close();

        } catch (JAXBException exception) {
            Logger.getLogger(XMLDataManager.class.getName()).
                    log(Level.SEVERE, "marshallExample throw JAXBException", exception);
        } catch (XMLStreamException ex) {
            Logger.getLogger(XMLDataManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return users;
    }

}
