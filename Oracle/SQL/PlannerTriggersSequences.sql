


--Trigger event of the Full Name table

CREATE OR REPLACE TRIGGER "DML LISTNER INSERT FULL NAME"
AFTER INSERT
ON "Full Name"
FOR EACH ROW
DECLARE
BEGIN
    INSERT INTO "Full Name"("Full Name"."ID Full Name", "Full Name"."Name", "Full Name"."Surname", "Full Name"."Pathronymic")
      VALUES("ID Full Name sequence".NEXTVAL, :NEW."Name", :NEW."Surname", :NEW."Pathronymic");
END "DML LISTNER INSERT FULL NAME";


--Trigger event of the User table

CREATE OR REPLACE TRIGGER "DML LISTNER INSERT USER"
AFTER INSERT
ON "User"
FOR EACH ROW
DECLARE
BEGIN
    INSERT INTO "User"("User"."ID User", "User"."Login", "User"."ID Full Name", "User"."Password", "User"."ID Image")
      VALUES("ID User sequence".NEXTVAL, :NEW."Login", :NEW."ID Full Name", :NEW."Password", :NEW."ID Image");
END "DML LISTNER INSERT USER";


