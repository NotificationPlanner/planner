
--Insert Test Data

--Insert Full Name
DELETE FROM "Full Name";
INSERT INTO "Full Name" VALUES(000100, 'Max', 'Marmonov', 'Perovich');
INSERT INTO "Full Name" VALUES(000101, 'Mikhail', 'Parhomov', 'Ivanovich');
INSERT INTO "Full Name" VALUES(000102, 'Olga', 'Deryabkina', 'Petrovna');
INSERT INTO "Full Name" VALUES(000103, 'Alexander', 'Kotov', 'Alexandrovich');
INSERT INTO "Full Name" VALUES(000104, 'Natasha', 'Yashanova', 'Sergeevna');
INSERT INTO "Full Name" VALUES(000105, 'Maxim', 'Korshonov', 'Perovich');
INSERT INTO "Full Name" VALUES(000106, 'Angela', 'Marmonova', 'Olegovna');
INSERT INTO "Full Name" VALUES(000107, 'Elena', 'Popova', 'Vladimirovna');
INSERT INTO "Full Name" VALUES(000108, 'Konstantin', 'Poroshkov', 'Ivanovich');
INSERT INTO "Full Name" VALUES(000109, 'Inna', 'Proshina', 'Egorovna');


--Insert User
DELETE FROM "User";
INSERT INTO "User" VALUES(100100, 'Max', 000100, 'Max', NULL);
INSERT INTO "User" VALUES(100101, 'Fox', 000101, 'Fox', NULL);
INSERT INTO "User" VALUES(100102, 'Olga', 000102, 'Olga', NULL);
INSERT INTO "User" VALUES(100103, 'Alex', 000103, 'Alex', NULL);
INSERT INTO "User" VALUES(100104, 'Flash', 000104, 'Flash', NULL);
INSERT INTO "User" VALUES(100105, 'Pox', 000105, 'Pox', NULL);
INSERT INTO "User" VALUES(100106, 'Fix', 000106, 'Fix', NULL);
INSERT INTO "User" VALUES(100107, 'Six', 000107, 'Six', NULL);
INSERT INTO "User" VALUES(100108, 'Roll', 000108, 'Roll', NULL);
INSERT INTO "User" VALUES(100109, 'Wolf', 000109, 'Wolf', NULL);


--Insert Category
DELETE FROM "Category";
INSERT INTO "Category" VALUES(220000, 'Homework');
INSERT INTO "Category" VALUES(220001, 'Birthday');
INSERT INTO "Category" VALUES(220002, 'Job');
INSERT INTO "Category" VALUES(220003, 'Hobby');
INSERT INTO "Category" VALUES(220004, 'Holiday');
INSERT INTO "Category" VALUES(220005, 'Outdoor');
INSERT INTO "Category" VALUES(220006, 'Plan of the day');


--Insert Task
DELETE FROM "Task";
INSERT INTO "Task" VALUES(200000, 'Homwork of the 10.10.2010', TO_DATE('9-10-2010','DD-MM-YYYY'), 'Mathematics, Geography, Physics, Biology', 'off', 220000, null, null);
INSERT INTO "Task" VALUES(200001, 'Birthday my brother', TO_DATE('19-12-2015','DD-MM-YYYY'), 'Buy the toy', 'on', 220001, null, null);
INSERT INTO "Task" VALUES(200002, 'The star job in new year 2015', TO_DATE('11-01-2016','DD-MM-YYYY'), 'Let is start', 'on', 220002, null, null);
INSERT INTO "Task" VALUES(200003, 'Arduino', TO_DATE('3-01-2016','DD-MM-YYYY'), 'Fist steps', 'on', 220003, null, null);
INSERT INTO "Task" VALUES(200004, 'Market', TO_DATE('10-12-2015','DD-MM-YYYY'), 'Groats, meat, water', 'off', 220004, null, null);
INSERT INTO "Task" VALUES(200005, 'Shiryaevo', TO_DATE('9-10-2010','DD-MM-YYYY'), 'Mathematics, Geography, Physics, Biology', 'on', 220005, null, null);
INSERT INTO "Task" VALUES(200006, 'Plan of the day', TO_DATE('20-12-2015','DD-MM-YYYY'), 'Stand up and do labs', 'on', 220005, null, null);
INSERT INTO "Task" VALUES(200007, 'My day', TO_DATE('01-01-2015','DD-MM-YYYY'), 'Not working!!', 'off', 220001, null, null);
INSERT INTO "Task" VALUES(200008, 'Holidays!!', TO_DATE('01-01-2014','DD-MM-YYYY'), null, 'off', 220001, null, null);


--Insert Task User
DELETE FROM "Task User";
INSERT INTO "Task User" VALUES(200000, 100100, 'out');
INSERT INTO "Task User" VALUES(200001, 100102, 'out');
INSERT INTO "Task User" VALUES(200002, 100102, 'out');
INSERT INTO "Task User" VALUES(200002, 100103, 'out');
INSERT INTO "Task User" VALUES(200002, 100100, 'out');
INSERT INTO "Task User" VALUES(200004, 100104, 'out');
INSERT INTO "Task User" VALUES(200004, 100105, 'out');
INSERT INTO "Task User" VALUES(200005, 100106, 'out');
INSERT INTO "Task User" VALUES(200005, 100107, 'out');
INSERT INTO "Task User" VALUES(200005, 100108, 'out');
INSERT INTO "Task User" VALUES(200006, 100106, 'out');
INSERT INTO "Task User" VALUES(200007, 100107, 'out');
INSERT INTO "Task User" VALUES(200007, 100108, 'out');
INSERT INTO "Task User" VALUES(200008, 100108, 'out');





















