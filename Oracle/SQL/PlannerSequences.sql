

--Full Name sequence
DROP SEQUENCE "ID Full Name sequence";

CREATE SEQUENCE "ID Full Name sequence"
  INCREMENT BY 1
    START WITH 100
  MAXVALUE 1000
    NOCACHE
  NOCYCLE;  


--User sequence
DROP SEQUENCE "ID User sequence";

CREATE SEQUENCE "ID User sequence"
  INCREMENT BY 1
    START WITH 100100
  MAXVALUE 101000
    NOCACHE
  NOCYCLE;  


--Category sequence
DROP SEQUENCE "ID Category sequence";

CREATE SEQUENCE "ID Category sequence"
  INCREMENT BY 1
    START WITH 220000
  MAXVALUE 229999
    NOCACHE
  NOCYCLE;  


--Task sequence
DROP SEQUENCE "ID Task sequence";

CREATE SEQUENCE "ID Task sequence"
  INCREMENT BY 1
    START WITH 200000
  MAXVALUE 219999
    NOCACHE
  NOCYCLE;  

