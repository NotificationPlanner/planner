
--Drops
DROP TABLE "User" CASCADE CONSTRAINTS PURGE;
DROP TABLE "Full Name" CASCADE CONSTRAINTS PURGE;
DROP TABLE "File" CASCADE CONSTRAINTS PURGE;
DROP TABLE "Task User" CASCADE CONSTRAINTS PURGE;
DROP TABLE "Task" CASCADE CONSTRAINTS PURGE;
DROP TABLE "Category" CASCADE CONSTRAINTS PURGE;

--Create Tables
CREATE TABLE "File" ("ID File" NUMERIC, "Title" VARCHAR2(1000),  "Path" VARCHAR2(1000),
                   CONSTRAINT "File ID File PK" PRIMARY KEY("ID File"),
                   CONSTRAINT "File Path UK" UNIQUE("Path"));
                   
CREATE TABLE "Full Name" ("ID Full Name" NUMERIC, "Name" VARCHAR2(25),  "Surname" VARCHAR2(50), "Pathronymic" VARCHAR2(35),
                   CONSTRAINT "User ID Full Name PK" PRIMARY KEY("ID Full Name"));

CREATE TABLE "User" ("ID User" NUMERIC, "Login" VARCHAR2(25), "ID Full Name" NUMERIC NOT NULL, "Password" VARCHAR2(25),  "ID Image" NUMERIC,
                   CONSTRAINT "User ID User PK" PRIMARY KEY("ID User"),  
                   CONSTRAINT "User Login UK" UNIQUE("Login"),
                   CONSTRAINT "User ID Full Name FK" FOREIGN KEY("ID Full Name") REFERENCES "Full Name"("ID Full Name"),
                   CONSTRAINT "User ID Image FK" FOREIGN KEY("ID Image") REFERENCES "File"("ID File"));

CREATE TABLE "Category" ("ID Category" NUMERIC, "Title" VARCHAR2(150),
                        CONSTRAINT "Category ID Category PK" PRIMARY KEY("ID Category"));
                            
CREATE TABLE "Task" ("ID Task" NUMERIC, "Title" VARCHAR2(300), "Date" DATE, "Data" VARCHAR2(1800), "Notification Status" VARCHAR2(3) NOT NULL, 
                     "ID Category" NUMERIC, "ID Theme" NUMERIC, "ID Alarm" NUMERIC,
                   CONSTRAINT "Task ID Task PK" PRIMARY KEY("ID Task"),  
                   CONSTRAINT "Task ID Category FK" FOREIGN KEY("ID Category") REFERENCES "Category"("ID Category"),
                   CONSTRAINT "Task ID Theme FK" FOREIGN KEY("ID Theme") REFERENCES "File"("ID File"),
                   CONSTRAINT "Task ID Alarm FK" FOREIGN KEY("ID Alarm") REFERENCES "File"("ID File"));
                   
CREATE TABLE "Task User" ("ID Task" NUMERIC, "ID User" NUMERIC, "Recycle bin" VARCHAR2(3) NOT NULL,
                        CONSTRAINT "Task User ID Task User PK" PRIMARY KEY("ID Task","ID User"),
                        CONSTRAINT "Task User ID Task FK" FOREIGN KEY("ID Task") REFERENCES "Task"("ID Task"),  
                        CONSTRAINT "Task User ID User FK" FOREIGN KEY("ID User") REFERENCES "User"("ID User"));
