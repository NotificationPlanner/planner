/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functional_layer.task;

import functional_layer.task.Command.CommandTask;

/**
 *
 * @author 2882_tsukanov
 */
public class ControlTaskUnit {
    private CommandTask commandTask;
    
    public void setCommand(CommandTask commandTask) {
        this.commandTask = commandTask;
    }

    public void excecuteCommand() {
        commandTask.excecute();
    }
}
