/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functional_layer.task.Command;

import functional_layer.task.ActionEngineTask;

/**
 *
 * @author 2882_tsukanov
 */
public class ViewTasks extends CommandTask {

    public ViewTasks(ActionEngineTask actionEngineTask) {
        this.actionEngineTask = actionEngineTask;
    }

    @Override
    public void excecute() {
        actionEngineTask.run(this);
    }

}
