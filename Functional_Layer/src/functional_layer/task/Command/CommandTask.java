/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functional_layer.task.Command;

import data_layer.Task;
import functional_layer.task.ActionEngineTask;

/**
 *
 * @author 2882_tsukanov
 */
public abstract class CommandTask {

    protected Task task;

    protected ActionEngineTask actionEngineTask;

    public abstract void excecute();

    public Task getTask() {
        return task;
    }

}
