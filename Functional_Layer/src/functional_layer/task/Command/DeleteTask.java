/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functional_layer.task.Command;

import data_layer.Task;
import functional_layer.task.ActionEngineTask;

/**
 *
 * @author 2882_tsukanov
 */
public class DeleteTask extends CommandTask {

    public DeleteTask(ActionEngineTask actionEngineTask, Task task) {
        this.task = task;
        this.actionEngineTask = actionEngineTask;
    }

    @Override
    public void excecute() {
        actionEngineTask.run(this);
    }

}
