/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functional_layer.task;

import data_layer.Task;
import data_layer.User;
import dbms_layer.DataManager;
import functional_layer.Planner;
import functional_layer.task.Command.CommandTask;
import functional_layer.task.Command.DeleteTask;
import functional_layer.task.Command.ViewTask;
import functional_layer.task.Command.ViewTasks;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author 2882_tsukanov
 */
public class ActionEngineTask {

    private Task task;
    private List<Task> tasks;

    public void run(CommandTask commandTask) {
        if (commandTask instanceof ViewTask) {
            DataManager dataManager = Planner.getDataManager();
            Iterator iterator = dataManager.selectUser().getTasks().iterator();
            while (iterator.hasNext()) {
                Task task = (Task) iterator.next();
                if (task.getTaskId().getId().equals(commandTask.getTask().getTaskId().getId())) {
                    this.task = task;
                    break;
                }
            }
        } else if (commandTask instanceof ViewTasks) {
            DataManager dataManager = Planner.getDataManager();
            tasks = dataManager.selectTasks();
        } else if (commandTask instanceof DeleteTask) {
            DataManager dataManager = Planner.getDataManager();
            User user = dataManager.selectUser();
            Iterator iterator = dataManager.selectUser().getTasks().iterator();
            while (iterator.hasNext()) {
                if (task.getTaskId().getId().equals(commandTask.getTask().getTaskId().getId())) {
                    iterator.next();
                    iterator.remove();
                    break;
                }
                iterator.next();

            }
            user.setTasks((List<Task>) iterator);
            dataManager.createUser(user);
        }
    }

    public Task getTask() {
        return task;
    }

    public List<Task> getTasks() {
        return tasks;
    }

}
