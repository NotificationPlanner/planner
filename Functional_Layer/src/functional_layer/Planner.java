/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functional_layer;

import data_layer.Task;
import data_layer.User;
import dbms_layer.DataManager;
import dbms_layer.realization.SQLDataManager;
import dbms_layer.realization.XMLDataManager;
import functional_layer.connect_task.ActionEngineConnectTask;
import functional_layer.connect_task.Command.CreateTask;
import functional_layer.connect_task.Command.CreateTasks;
import functional_layer.connect_task.ControlConnectTaskUnit;
import functional_layer.task.ActionEngineTask;
import functional_layer.task.Command.DeleteTask;
import functional_layer.task.Command.ViewTask;
import functional_layer.task.Command.ViewTasks;
import functional_layer.task.ControlTaskUnit;
import functional_layer.user.ActionEngineUser;
import functional_layer.user.Command.CreateUser;
import functional_layer.user.Command.CreateUsers;
import functional_layer.user.Command.ViewUser;
import functional_layer.user.Command.ViewUsers;
import functional_layer.user.ControlUserUnit;
import java.util.List;

/**
 *
 * @author 2882_tsukanov
 */
public class Planner {

    private static DataManager dataManager;
    ActionEngineConnectTask actionEngineConnectTask;
    ControlConnectTaskUnit controlConnectTaskUnit;
    ActionEngineTask actionEngineTask;
    ControlTaskUnit controlTaskUnit;
    ActionEngineUser actionEngineUser;
    ControlUserUnit controlUserUnit;

    public Planner(DBMS dbms) {
        switch (dbms) {
            case XML:
                dataManager = new DataManager(new XMLDataManager());
                break;
            case SQL:
                throw new ExceptionInInitializerError();
        }
        actionEngineConnectTask = new ActionEngineConnectTask();
        controlConnectTaskUnit = new ControlConnectTaskUnit();

        actionEngineTask = new ActionEngineTask();
        controlTaskUnit = new ControlTaskUnit();

        actionEngineUser = new ActionEngineUser();
        controlUserUnit = new ControlUserUnit();
    }

    public void createTask(Task task, User user) {
        controlConnectTaskUnit.setCommand(new CreateTask(actionEngineConnectTask, task, user));
        controlConnectTaskUnit.excecuteCommand();
    }

    public void createTasks(List<Task> tasks, User user) {
        controlConnectTaskUnit.setCommand(new CreateTasks(actionEngineConnectTask, tasks, user));
        controlConnectTaskUnit.excecuteCommand();
    }

    public void createUser(User user) {
        controlUserUnit.setCommand(new CreateUser(actionEngineUser, user));
        controlUserUnit.excecuteCommand();
    }

    public void createUsers(List<User> users) {
        controlUserUnit.setCommand(new CreateUsers(actionEngineUser, users));
        controlUserUnit.excecuteCommand();
    }

    public Task viewTask(Task task) {
        controlTaskUnit.setCommand(new ViewTask(actionEngineTask, task));
        controlTaskUnit.excecuteCommand();
        return actionEngineTask.getTask();
    }

    public List<Task> viewTasks() {
        controlTaskUnit.setCommand(new ViewTasks(actionEngineTask));
        controlTaskUnit.excecuteCommand();
        return actionEngineTask.getTasks();
    }

    public List<User> viewUsers() {
        controlUserUnit.setCommand(new ViewUsers(actionEngineUser));
        controlUserUnit.excecuteCommand();
        return actionEngineUser.getUsers();
    }

    public User viewUser(User user) {
        controlUserUnit.setCommand(new ViewUser(actionEngineUser, user));
        controlUserUnit.excecuteCommand();
        return actionEngineUser.getUser();
    }

    public void deleteTask(Task task) {
        controlTaskUnit.setCommand(new DeleteTask(actionEngineTask, task));
        controlTaskUnit.excecuteCommand();
    }

    public static DataManager getDataManager() {
        return dataManager;
    }

}
