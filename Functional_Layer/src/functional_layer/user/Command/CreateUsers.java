/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functional_layer.user.Command;

import data_layer.User;
import functional_layer.user.ActionEngineUser;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLStreamException;

/**
 *
 * @author 2882_tsukanov
 */
public class CreateUsers extends CommandUser {

    public CreateUsers(ActionEngineUser actionEngineUser, List<User> users) {
        this.users = users;
        this.actionEngineUser = actionEngineUser;
    }

    @Override
    public void excecute() {
        try {
            actionEngineUser.run(this);
        } catch (IOException ex) {
            Logger.getLogger(CreateUsers.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XMLStreamException ex) {
            Logger.getLogger(CreateUsers.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
