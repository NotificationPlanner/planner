/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functional_layer.user.Command;

import data_layer.User;
import functional_layer.user.ActionEngineUser;
import java.util.List;

/**
 *
 * @author 2882_tsukanov
 */
public abstract class CommandUser {

    protected ActionEngineUser actionEngineUser;
    protected User user;
    protected List<User> users;

    public abstract void excecute();

    public User getUser() {
        return user;
    }

    public List<User> getUsers() {
        return users;
    }
}
