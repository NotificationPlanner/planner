/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functional_layer.user.Command;

import data_layer.User;
import functional_layer.user.ActionEngineUser;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLStreamException;

/**
 *
 * @author 2882_tsukanov
 */
public class CreateUser extends CommandUser {

    public CreateUser(ActionEngineUser actionEngineUser, User user) {
        this.user = user;
        this.actionEngineUser = actionEngineUser;
    }

    @Override
    public void excecute() {
        try {
            actionEngineUser.run(this);
        } catch (IOException ex) {
            Logger.getLogger(CreateUser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XMLStreamException ex) {
            Logger.getLogger(CreateUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
