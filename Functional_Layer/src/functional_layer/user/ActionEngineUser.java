/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functional_layer.user;

import data_layer.User;
import dbms_layer.DataManager;
import functional_layer.Planner;
import functional_layer.user.Command.CommandUser;
import functional_layer.user.Command.CreateUser;
import functional_layer.user.Command.CreateUsers;
import functional_layer.user.Command.ViewUser;
import functional_layer.user.Command.ViewUsers;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import javax.xml.stream.XMLStreamException;

/**
 *
 * @author 2882_tsukanov
 */
public class ActionEngineUser {

    private User user;
    private List<User> users;

    public void run(CommandUser commandUser) throws IOException, FileNotFoundException, XMLStreamException {
        //Not add User
        if (commandUser instanceof ViewUser) {
            DataManager dataManager = Planner.getDataManager();
            user = dataManager.selectUser();
        } else if (commandUser instanceof ViewUsers) {
            DataManager dataManager = Planner.getDataManager();
            users = dataManager.selectUsers();
        } else if (commandUser instanceof CreateUser) {
            DataManager dataManager = Planner.getDataManager();
            dataManager.createUser(commandUser.getUser());
        } else if (commandUser instanceof CreateUsers) {
            DataManager dataManager = Planner.getDataManager();
            dataManager.createUsers(commandUser.getUsers());
        }
    }

    public User getUser() {
        return user;
    }

    public List<User> getUsers() {
        return users;
    }
    
}
