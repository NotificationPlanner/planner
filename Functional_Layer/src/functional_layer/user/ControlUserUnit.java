/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functional_layer.user;

import functional_layer.user.Command.CommandUser;

/**
 *
 * @author 2882_tsukanov
 */
public class ControlUserUnit {

    private CommandUser commandUser;

    public void setCommand(CommandUser commandUser) {
        this.commandUser = commandUser;
    }

    public void excecuteCommand() {
        commandUser.excecute();
    }
}
