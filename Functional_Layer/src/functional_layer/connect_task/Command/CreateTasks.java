/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functional_layer.connect_task.Command;

import data_layer.Task;
import data_layer.User;
import functional_layer.connect_task.ActionEngineConnectTask;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLStreamException;

/**
 *
 * @author 2882_tsukanov
 */
public class CreateTasks extends CommandTaskConnect {

    public CreateTasks(ActionEngineConnectTask actionEngineConnectTask, List<Task> tasks, User user) {
        this.tasks = tasks;
        this.user = user;
        this.actionEngineConnectTask = actionEngineConnectTask;
    }

    @Override
    public void excecute() {
        try {
            actionEngineConnectTask.run(this);
        } catch (IOException ex) {
            Logger.getLogger(CreateTasks.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XMLStreamException ex) {
            Logger.getLogger(CreateTasks.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
