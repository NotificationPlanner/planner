/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functional_layer.connect_task.Command;

import data_layer.Task;
import data_layer.User;
import functional_layer.connect_task.ActionEngineConnectTask;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLStreamException;

/**
 *
 * @author 2882_tsukanov
 */
public class CreateTask extends CommandTaskConnect {

    public CreateTask(ActionEngineConnectTask actionEngineConnectTask, Task task, User user) {
        this.task = task;
        this.user = user;
        this.actionEngineConnectTask = actionEngineConnectTask;
    }

    @Override
    public void excecute() {
        try {
            actionEngineConnectTask.run(this);
        } catch (IOException ex) {
            Logger.getLogger(CreateTask.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XMLStreamException ex) {
            Logger.getLogger(CreateTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
