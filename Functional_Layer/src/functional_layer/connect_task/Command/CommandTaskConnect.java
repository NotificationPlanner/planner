/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functional_layer.connect_task.Command;

import data_layer.Task;
import data_layer.User;
import functional_layer.connect_task.ActionEngineConnectTask;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 2882_tsukanov
 */
public abstract class CommandTaskConnect {

    protected ActionEngineConnectTask actionEngineConnectTask;
    protected Task task;
    protected List<Task> tasks;
    protected User user;

    public abstract void excecute();

    public Task getTask() {
        return task;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public User getUser() {
        return user;
    }

}
