/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functional_layer.connect_task;

import functional_layer.connect_task.Command.CommandTaskConnect;

/**
 *
 * @author 2882_tsukanov
 */
public class ControlConnectTaskUnit {

    private CommandTaskConnect commandTaskConnect;

    public void setCommand(CommandTaskConnect commandTaskConnect) {
        this.commandTaskConnect = commandTaskConnect;
    }

    public void excecuteCommand() {
        commandTaskConnect.excecute();
    }
}
