/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functional_layer.connect_task;

import data_layer.Task;
import dbms_layer.DataManager;
import functional_layer.Planner;
import functional_layer.connect_task.Command.CommandTaskConnect;
import functional_layer.connect_task.Command.CreateTask;
import functional_layer.connect_task.Command.CreateTasks;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import javax.xml.stream.XMLStreamException;

/**
 *
 * @author 2882_tsukanov
 */
public class ActionEngineConnectTask {

    public void run(CommandTaskConnect commandTaskConnect) throws IOException, FileNotFoundException, XMLStreamException {
        //Not add User
        if (commandTaskConnect instanceof CreateTask) {
            DataManager dataManager = Planner.getDataManager();
            commandTaskConnect.getTask().setUserId(commandTaskConnect.getUser().getUserId());
            dataManager.createTask(commandTaskConnect.getTask());
        }
        else if (commandTaskConnect instanceof CreateTasks) {
            Iterator iterator = commandTaskConnect.getTasks().iterator();
            while (iterator.hasNext()) {
                Task task = (Task) iterator.next();
                task.setUserId(commandTaskConnect.getUser().getUserId());
            }
            DataManager dataManager = Planner.getDataManager();
            dataManager.createTasks(commandTaskConnect.getTasks());
        }
    }
}
