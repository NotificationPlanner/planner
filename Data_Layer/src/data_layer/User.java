/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data_layer;

import data_layer.user.FullName;
import data_layer.user.UserAccount;
import data_layer.user.UserId;
import data_layer.user.UserPassword;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author 2882_tsukanov
 */
@XmlType(propOrder = {"userId", "userPassword", "userAccount", "fullName", "tasks",}, name = "User")
@XmlRootElement
public class User {

    private UserId userId;
    private UserPassword userPassword;
    private UserAccount userAccount;
    private FullName fullName;
    private List<Task> tasks = new LinkedList<>();
    //private Image image;

    public User() {
    }

    public User(UserId userId, UserPassword userPassword, UserAccount userAccount, FullName fullName) {
        this.userId = userId;
        this.userPassword = userPassword;
        this.userAccount = userAccount;
        this.fullName = fullName;
        //this.image = image;

    }

    public User(BigDecimal userId, String userPassword, String userAccount, String name, String surname, String pathronymic) {
        this.userId = new UserId(userId);
        this.userPassword = new UserPassword(userPassword);
        this.userAccount = new UserAccount(userPassword);
        this.fullName = new FullName(userId, name, surname, pathronymic);
        //this.image = image;

    }

    /**
     * @return the userId
     */
    public UserId getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(UserId userId) {
        this.userId = userId;
    }

    /**
     * @return the userPassword
     */
    public UserPassword getUserPassword() {
        return userPassword;
    }

    /**
     * @param userPassword the userPassword to set
     */
    public void setUserPassword(UserPassword userPassword) {
        this.userPassword = userPassword;
    }

    /**
     * @return the userAccount
     */
    public UserAccount getUserAccount() {
        return userAccount;
    }

    /**
     * @param userAccount the userAccount to set
     */
    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    /**
     * @return the fullName
     */
    public FullName getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(FullName fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the tasks
     */
    public List<Task> getTasks() {
        return tasks;
    }

    /**
     * @param tasks the tasks to set
     */
    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}
