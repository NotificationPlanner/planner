/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data_layer.file;

import data_layer.File;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author 2882_tsukanov
 */
@XmlType(propOrder = {"idFile", "title", "path"}, name = "File")
public class Image extends File {

    public Image() {
        super();
    }

    public Image(BigDecimal idFile, String title, String path) {
        super(idFile, title, path);
    }

}
