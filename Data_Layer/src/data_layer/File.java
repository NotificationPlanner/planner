/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data_layer;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author 2882_tsukanov
 */
@XmlType(propOrder = { "idFile", "title","path"}, name = "File")
public abstract class File {

    private BigDecimal idFile;
    private String title;
    private String path;

    public File() {
    }

    public File(BigDecimal idFile, String title, String path) {
        this.idFile = idFile;
        this.title = title;
        this.path = path;
    }

    /**
     * @return the idFile
     */
    public BigDecimal getIdFile() {
        return idFile;
    }

    /**
     * @param idFile the idFile to set
     */
    public void setIdFile(BigDecimal idFile) {
        this.idFile = idFile;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }
}
