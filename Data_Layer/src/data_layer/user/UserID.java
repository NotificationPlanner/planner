/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data_layer.user;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author 2882_tsukanov
 */
@XmlType(propOrder = {"id"}, name = "UserId")
public class UserId {

    private BigDecimal id;

    public UserId() {
    }

    public UserId(BigDecimal id) {
        this.id = id;
    }

    /**
     * @return the idUser
     */
    public BigDecimal getId() {
        return id;
    }
    
    public void setId(BigDecimal id) {
        this.id = id;
    }
}
