/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data_layer.user;

import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author 2882_tsukanov
 */
@XmlType(propOrder = {"account"}, name = "UserAccount")
public class UserAccount {

    private String account;

    public UserAccount() {
    }

    public UserAccount(String account) {
        this.account = account;
    }

    /**
     * @return the account
     */
    public String getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(String account) {
        this.account = account;
    }
}
