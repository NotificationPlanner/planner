/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data_layer.user;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author 2882_tsukanov
 */
@XmlType(propOrder = {"idFullName", "name", "surname", "pathronymic"}, name = "FullName")
public class FullName {

    private BigDecimal idFullName;
    private String name;
    private String surname;
    private String pathronymic;

    public FullName() {
    }

    public FullName(BigDecimal idFullName, String name, String surname, String pathronymic) {
        this.idFullName = idFullName;
        this.name = name;
        this.surname = surname;
        this.pathronymic = pathronymic;
    }

    /**
     * @return the idFullName
     */
    public BigDecimal getIdFullName() {
        return idFullName;
    }

    /**
     * @param idFullName the idFullName to set
     */
    public void setIdFullName(BigDecimal idFullName) {
        this.idFullName = idFullName;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * @param surname the surname to set
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * @return the pathronymic
     */
    public String getPathronymic() {
        return pathronymic;
    }

    /**
     * @param pathronymic the pathronymic to set
     */
    public void setPathronymic(String pathronymic) {
        this.pathronymic = pathronymic;
    }
}
