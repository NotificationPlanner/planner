/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data_layer.task;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author 2882_tsukanov
 */
@XmlType(propOrder = {"id"}, name = "TaskId")
public class TaskId {

    private BigDecimal id;

    public TaskId() {
    }

    public TaskId(BigDecimal id) {
        this.id = id;
    }

    /**
     * @return the idTask
     */
    public BigDecimal getId() {
        return id;
    }

    /**
     * @param idTask the idTask to set
     */
    public void setId(BigDecimal id) {
        this.id = id;
    }

}
