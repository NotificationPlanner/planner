/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data_layer.task;

import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author 2882_tsukanov
 */
@XmlType(propOrder = { "title" }, name = "TaskTitle")
public class TaskTitle {

    private String title;

    public TaskTitle() {
    }

    public TaskTitle(String title) {
        this.title = title;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
}
