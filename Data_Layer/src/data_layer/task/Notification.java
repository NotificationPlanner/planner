/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data_layer.task;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 *
 * @author 2882_tsukanov
 */
public enum Notification {
    @XmlEnumValue(value = "On")
    On,
    @XmlEnumValue(value = "Off")
    Off
}
