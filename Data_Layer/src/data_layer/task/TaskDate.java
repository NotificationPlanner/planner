/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data_layer.task;

import java.util.Date;

import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author 2882_tsukanov
 */
@XmlType(propOrder = { "date" }, name = "TaskDate")
public class TaskDate {

    private Date date;

    public TaskDate() {
    }

    public TaskDate(Date date) {
        this.date = date;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }
}
