/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data_layer;

import data_layer.task.Notification;
import data_layer.task.TaskData;
import data_layer.task.TaskDate;
import data_layer.task.TaskId;
import data_layer.task.TaskTitle;
import data_layer.user.UserId;
import java.math.BigDecimal;
import java.util.Date;
import javax.xml.bind.annotation.XmlAttribute;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author 2882_tsukanov
 */
@XmlType(propOrder = {"taskId", "userId", "taskTitle", "taskDate", "taskData"}, name = "Task")
@XmlRootElement
public class Task {

    private TaskId taskId;
    private UserId userId;
    @XmlAttribute(name = "Notification")
    private Notification notification;
    private TaskTitle taskTitle;
    private TaskDate taskDate;
    private TaskData taskData;
//    private Alarm alarm;
//    private Image image;
//    private Theme theme;

    public Task() {
    }

    public Task(BigDecimal taskId) {
        this.taskId = new TaskId(taskId);
    }

    public Task(BigDecimal taskId, BigDecimal userId, Notification notification, String taskTitle, Date taskDate, String taskData) {
        this.taskId = new TaskId(taskId);
        this.userId = new UserId(userId);
        this.notification = notification;
        this.taskTitle = new TaskTitle(taskTitle);
        this.taskDate = new TaskDate(taskDate);
        this.taskData = new TaskData(taskData);
    }

    public Task(TaskId taskId, UserId userId, Notification notification, TaskTitle taskTitle, TaskDate taskDate, TaskData taskData) {
        this.taskId = taskId;
        this.userId = userId;
        this.notification = notification;
        this.taskTitle = taskTitle;
        this.taskDate = taskDate;
        this.taskData = taskData;
    }

    public Task(TaskId taskId, User user, Notification notification, TaskTitle taskTitle, TaskDate taskDate, TaskData taskData) {
        this.taskId = taskId;
        this.userId = user.getUserId();
        this.notification = notification;
        this.taskTitle = taskTitle;
        this.taskDate = taskDate;
        this.taskData = taskData;
    }

    public UserId getUserId() {
        return userId;
    }

    public void setUserId(UserId userId) {
        this.userId = userId;
    }

    public Notification getNotification() {
        return notification;
    }

    public Task Notification_Off() {
        notification = Notification.Off;
        return this;
    }

    public Task Notification_On() {
        notification = Notification.On;
        return this;
    }

    /**
     * @return the taskID
     */
    public TaskId getTaskId() {
        return taskId;
    }

    /**
     * @param taskID the taskID to set
     */
    public void setTaskId(TaskId taskId) {
        this.taskId = taskId;
    }

    /**
     * @return the taskTitle
     */
    public TaskTitle getTaskTitle() {
        return taskTitle;
    }

    /**
     * @param taskTitle the taskTitle to set
     */
    public void setTaskTitle(TaskTitle taskTitle) {
        this.taskTitle = taskTitle;
    }

    /**
     * @return the taskDate
     */
    public TaskDate getTaskDate() {
        return taskDate;
    }

    /**
     * @param taskDate the taskDate to set
     */
    public void setTaskDate(TaskDate taskDate) {
        this.taskDate = taskDate;
    }

    /**
     * @return the taskData
     */
    public TaskData getTaskData() {
        return taskData;
    }

    /**
     * @param taskData the taskData to set
     */
    public void setTaskData(TaskData taskData) {
        this.taskData = taskData;
    }

}
