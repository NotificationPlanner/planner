package view;

import model.datalayer.Task;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Вячеслав on 23.11.2015.
 */
public class PlannerView extends JFrame {

    private JMenuBar menuBar;
    private JMenu menuLogIn;
    private JMenuItem menuItemLogIn[];
    private JMenu menuFile;
    private JMenuItem menuItemFile[];
    private JMenu menuEdit;
    private JMenuItem menuItemEdit[];
    private JMenu menuView;
    private JMenuItem menuItemView[];
    private JList<Task> listTask;
    private JScrollPane scroll;
    private JButton addButton;
    private JButton changeButton;
    private JButton delButton;
    private JLabel labelTask;
    private JPanel panel;

    public PlannerView(String title) {

        super(title);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(370, 570);
        this.setResizable(false);

        panel = new JPanel();
        menuBar = new JMenuBar();
        //Меню вход
        menuLogIn = new JMenu("Вход");
        menuItemLogIn = new JMenuItem[3];
        menuItemLogIn[0] = new JMenuItem("Авторизация");
        menuItemLogIn[1] = new JMenuItem("Выйти");
        menuItemLogIn[1].setEnabled(false);
        menuLogIn.add(menuItemLogIn[0]);
        menuLogIn.add(menuItemLogIn[1]);
        menuLogIn.addSeparator();
        menuItemLogIn[2] = new JMenuItem("Регистрация");
        menuLogIn.add(menuItemLogIn[2]);
        //Меню Файл
        menuFile = new JMenu("Файл");
        menuItemFile = new JMenuItem[5];
        menuItemFile[0] = new JMenuItem("Создать");
        menuItemFile[1] = new JMenuItem("Открыть");
        menuItemFile[2] = new JMenuItem("Сохранить");
        menuItemFile[3] = new JMenuItem("Сохранить как");
        menuItemFile[0].setEnabled(false);
        menuItemFile[1].setEnabled(false);
        menuItemFile[2].setEnabled(false);
        menuItemFile[3].setEnabled(false);
        menuFile.add(menuItemFile[0]);
        menuFile.add(menuItemFile[1]);
        menuFile.add(menuItemFile[2]);
        menuFile.add(menuItemFile[3]);
        menuFile.addSeparator();
        menuItemFile[4] = new JMenuItem("Выход");
        menuFile.add(menuItemFile[4]);
        // Меню редактировать
        menuEdit = new JMenu("Редактировать");
        menuEdit.setEnabled(false);
        menuItemEdit = new JMenuItem[5];
        menuItemEdit[0] = new JMenuItem("Добавить");
        menuItemEdit[1] = new JMenuItem("Удалить");
        menuItemEdit[2] = new JMenuItem("Изменить");
        menuEdit.add(menuItemEdit[0]);
        menuEdit.add(menuItemEdit[1]);
        menuEdit.add(menuItemEdit[2]);
        menuEdit.addSeparator();
        menuItemEdit[3] = new JMenuItem("Поиск");
        menuItemEdit[4] = new JMenuItem("Выделить все");
        menuEdit.add(menuItemEdit[3]);
        menuEdit.add(menuItemEdit[4]);
        // Меню вид
        menuView = new JMenu("Вид");
        menuView.setEnabled(false);
        menuItemEdit = new JMenuItem[3];
        menuItemEdit[0] = new JMenuItem("Все задачи");
        menuItemEdit[1] = new JMenuItem("Выполненные");
        menuItemEdit[2] = new JMenuItem("Текущие");
        menuEdit.add(menuItemEdit[0]);
        menuEdit.add(menuItemEdit[1]);
        menuEdit.add(menuItemEdit[2]);

        menuBar.add(menuLogIn);
        menuBar.add(menuFile);
        menuBar.add(menuEdit);
        menuBar.add(menuView);

        labelTask = new JLabel("Список задач:");
        panel.add(labelTask);

        listTask = new JList<Task>();
        scroll = new JScrollPane(listTask);
        scroll.setPreferredSize(new Dimension(350, 450));
        panel.add(scroll);

        addButton = new JButton("Добавить");
        delButton = new JButton("Удалить");
        changeButton = new JButton("Изменить");
        addButton.setPreferredSize(new Dimension(100, 25));
        delButton.setPreferredSize(new Dimension(100, 25));
        changeButton.setPreferredSize(new Dimension(100, 25));
        panel.add(addButton);
        panel.add(delButton);
        panel.add(changeButton);

        menuItemLogIn[0].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AuthorizationView authorizationView = new AuthorizationView("Вход");
                authorizationView.setVisible(true);
            }
        });

        this.setJMenuBar(menuBar);
        this.add(panel, BorderLayout.CENTER);
    }

}
