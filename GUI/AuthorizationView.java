package view;

import controller.PlannerController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Вячеслав on 23.11.2015.
 */
public class AuthorizationView extends JFrame {

    private JLabel login;
    private JLabel password;
    private JTextField inputLogin;
    private JPasswordField inputPassword;
    private JButton logIn;
    private PlannerController controller;

    public AuthorizationView(String title) {

        super(title);

        controller = PlannerController.getInstance();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setLocation(200,300);
        this.setSize(200, 155);
        this.setLayout(new FlowLayout());

        login = new JLabel("Логин:");
        password = new JLabel("Пароль:");

        inputLogin = new JTextField(15);
        inputPassword = new JPasswordField(15);

        logIn = new JButton("Ок");
        logIn.setPreferredSize(new Dimension(65,25));


        logIn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //controller.inputTask(inputLogin.getText(), String.valueOf(inputPassword.getPassword()));
                dispose();
            }
        });

        this.add(login);
        this.add(inputLogin);
        this.add(password);
        this.add(inputPassword);
        this.add(logIn);
    }
}
