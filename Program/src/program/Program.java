/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package program;

import data_layer.Task;
import data_layer.User;
import data_layer.task.Notification;
import data_layer.task.TaskData;
import data_layer.task.TaskDate;
import data_layer.task.TaskId;
import data_layer.task.TaskTitle;
import data_layer.user.UserId;
import dbms_layer.DataManager;
import dbms_layer.contract.IDataManager;
import dbms_layer.realization.XMLDataManager;
import functional_layer.DBMS;
import functional_layer.Planner;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.xml.stream.XMLStreamException;

/**
 *
 * @author 2882_tsukanov
 */
public class Program {

    //Create Data
    public static void main(String[] args) throws IOException, FileNotFoundException, XMLStreamException {
        Task task1 = new Task(
                new TaskId(BigDecimal.TEN),
                new UserId(BigDecimal.ONE),
                Notification.Off,
                new TaskTitle("Homework and go to school tomorrow"),
                new TaskDate(Date.from(Instant.now())),
                new TaskData("Go to school to morning"));
        Task task2 = new Task(
                new TaskId(BigDecimal.ZERO),
                new UserId(BigDecimal.ONE),
                Notification.Off,
                new TaskTitle("Homework"),
                new TaskDate(Date.from(Instant.now())),
                new TaskData("Go to school to morning"));

        User user1 = new User(BigDecimal.ONE, "userPassword", "userAccount", "name", "surname", "pathronymic");
        User user2 = new User(BigDecimal.ZERO, "userPassword", "userAccount", "name", "surname", "pathronymic");

        LinkedList<Task> tasksIn = new LinkedList<>();
        tasksIn.add(task1);
        tasksIn.add(task2);
        user1.setTasks(tasksIn);

        Planner planner = new Planner(DBMS.XML);//Pattern Command

        //Test create User
        planner.createUser(user1);
        //Test view User
        User userView = planner.viewUser(null);

        System.out.println("Objects created from XML:");
        System.out.println("User have ID = " + userView.getUserId().getId());
        System.out.println("User have tasks are id's: " + userView.getTasks().get(0).getTaskId().getId() + " and " + userView.getTasks().get(1).getTaskId().getId());

        //Test create Task
        planner.createTask(task2, user1);
        //Test view Task
        Task task = planner.viewTask(new Task(BigDecimal.ZERO));

        System.out.println("Objects created from XML:");
        System.out.println("Task have ID = " + task.getTaskId().getId() + ", notification is: " + task.getNotification());
        System.out.println("Task title is: " + task.getTaskTitle().getTitle() + " and user create with userID = " + task.getUserId().getId());

        //Test create Task
        //Not Worked
        //planner.deleteTask(task);
        //Test create Tasks
        planner.createTasks(tasksIn, user2);

        LinkedList<User> usersIn = new LinkedList<>();
        usersIn.add(user1);
        usersIn.add(user2);

        //Test create Users
        planner.createUsers(usersIn);

        List<Task> tasksOut = planner.viewTasks();
        System.out.println("Objects created from XML with viewTasks():");
        System.out.println("Size collection output view: " + tasksOut.size());

        List<User> usersOut = planner.viewUsers();
        System.out.println("Objects created from XMLwith viewUsers():");
        System.out.println("Size collection output view: " + usersOut.size());

        XMLDataManager dataManager = new XMLDataManager();

    }
}
